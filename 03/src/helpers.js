function holaMundo(){
    console.log("Hola Mundo");
}

var helloFn = function(){
    holaMundo();
}

var flechaFunction = () =>  helloFn();

const suma = (n1,n2) => n1 + n2;

const esPar = (n1) => {
    if(n1%2 === 0){
        console.log(`${n1} es par`);
    }else{
        console.log(`${n1} es impar`);
    }
}

exports.esPar = esPar;
exports.suma = suma;
exports.helloFn = helloFn;
exports.holaMundo = holaMundo;