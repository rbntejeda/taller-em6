try
{
    if(process.argv.length < 4)
    {
        throw "Faltan parametros";
    }
    let sum = 0;
    for (let i = 2; i < process.argv.length; i++) {
        sum+=parseInt(process.argv[i]);
    }
    const avg = sum / (process.argv.length -2);
    console.log(`El promedio es : ${avg}`);
} 
catch (error)
{
    console.error(error);
}