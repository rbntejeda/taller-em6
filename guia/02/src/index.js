try
{
    if(process.argv.length < 4)
    {
        throw "Faltan parametros";
    }
    let min = parseFloat(process.argv[2]);
    for (let i = 3; i < process.argv.length; i++) {
        const val = parseFloat(process.argv[i]);
        if(min > val)
        {
            min = val;
        }
    }
    console.log(`El menor valor es : ${min}`);
} 
catch (error)
{
    console.error(error);
}