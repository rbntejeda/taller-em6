try
{
    if(process.argv.length < 4)
    {
        throw "Faltan parametros";
    }
    let par = [];
    let impar = [];
    for (let i = 2; i < process.argv.length; i++) {
        const n1 = parseInt(process.argv[i]);
        if(n1%2 === 0){
            par.push(n1);
        }else{
            impar.push(n1);
        }
    }
    console.log(`Los valores pares son : [${par}] e impares son : [${impar}]`);
} 
catch (error)
{
    console.error(error);
}