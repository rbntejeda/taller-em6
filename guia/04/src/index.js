try
{
    if(process.argv.length != 3 )
    {
        throw "Parametros Invalidos";
    }
    let fact = parseInt(process.argv[2]);
    if(fact == 0)
        console.log("El factorial de 0 es : 1");
    else
    {
        let sum=1;
        while(fact!=1)
        {
            sum *= fact;
            fact--;
        }
        console.log(`El factorial de ${process.argv[2]} es : ${sum}`);
    }
} 
catch (error)
{
    console.error(error);
}