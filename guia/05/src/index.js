//Escribe una función que calcule el valor de una potencia, donde la base y el exponente son números enteros ingresados en la línea de comandos.

try
{
    if(process.argv.length != 4 )
    {
        throw "Parametros Invalidos";
    }
    const base = parseInt(process.argv[2]);
    const exponente = parseInt(process.argv[3]);
    const result = Math.pow(base,exponente);
    console.log(`El resultado de base ${base} y exponente ${exponente} el resultado es : ${result}`); 
} 
catch (error)
{
    console.error(error);
}