var _ = require('lodash');

const Exponencial = (base,exp) => {
    
    //return Math.pow(base,exp);
    if(exp==0){
        return 1;
    }
    else
    {
        if(exp>0)
        {
            let sum = base;
            for (let i = 1; i < exp; i++) 
            {
                sum *= base
            }
            return sum;
        }
        else
        {
            let sum = base;
            for (let i = 1; i < exp; i++) 
            {
                sum *= base;
            }
            return 1/sum;
        }
    }
}

const Factorial = (n) => {
    if(n === 0 ) return 1;
    return _.range(1, n+1)
        .reduce(function(p,i) { 
        return p*i;
        }
    );
}

exports.Exp = Exponencial;
exports.Fac = Factorial;