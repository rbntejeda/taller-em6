import { Exp,Fac } from './MyMath.Java';

try
{
    if(process.argv.length == 3)
    {
        let sum = 0;
        switch(process.argv[2])
        {
            case 'e':
                for (let i = 0; i < 20; i++) {
                    sum += Exp(Fac(i),-1);
                }
                console.log(`El valor de e = ${sum}`);
                break;
            case 'pi':
                for (let i = 0; i < 100000; i++) {
                    sum += 4*(Exp(-1,i)/(2*i+1));
                }
                console.log(`El valor de pi = ${sum}`);
                break;
            default:
                throw "Parametros Invalidos";
        }
    }
    else
    {
        if(process.argv.length == 4 && process.argv[2] == 'ln')
        {
            let x = parseFloat(process.argv[3]);
            let sum = 0;
            for (let i = 1; i < 100; i++) {
                sum += (Exp(-1,(i+1))/i)*Exp((x-1),i);                
            }
            console.log(`El ln(${x}) = ${sum}`);
        }
        else
        {
            throw "Parametros Invalidos";
        }
    }
} 
catch (error)
{
    console.error(error);
}