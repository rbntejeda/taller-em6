var _ = require('lodash');
let palabras = process.argv[2];
let inicio = parseInt(process.argv[3]);
let termino = parseInt(process.argv[4]);
if(inicio > termino || inicio <0 || inicio > palabras.length || termino < 0 || termino > palabras.length){
    console.log("Indice Invalido");
}else{
    console.log(palabras.substring(inicio,termino));
}