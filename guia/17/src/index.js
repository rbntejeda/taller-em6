var _ = require('lodash');
let palabras = process.argv[2].split(' ');
let palindromo = false;

let  reverseString = (str) => {
    var splitString = str.split("");
    var reverseArray = splitString.reverse(); 
    var joinArray = reverseArray.join("");
    return joinArray;
}

palabras.forEach(palabra => {
    if(palabra==reverseString(palabra))
    {
        palindromo = true;
    }
});

console.log(palindromo?"Es palindromo":"No es palindromo");
