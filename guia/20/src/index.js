var moment = require('moment');
moment.locale('es');
let fecha = process.argv[2];
if(moment(fecha).isValid())
{
    console.log(moment(fecha,'DD-MM-YYYY').format("LL"));
}
else
{
    console.log("Fecha no valida");
}